/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*- */
/*
 * gnome-keyring-query.c
 * Copyright (C) Luca Di Stefano 2004 <lucadistefano@tiscali.it>
 * 
 * main.c is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * main.c is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <termios.h>

#include <glib.h>
#include <gnome-keyring.h>

#include "config.h"

void version(){
	puts(PACKAGE_NAME " $Revision: 246 $\n");
}

void usage() {
	version();
	puts("Usage:\n"
		"    " PACKAGE_NAME " <mode> options\n"
	"Options:\n"
	"    --name     - username\n"
	"    --domain   - network domain\n"
	"    --passwod  - password\n"
	"    --ask      - ask confirmation\n"
	"    --help     - this\n"
	"Modes:\n"
	"    get    : gets the password of name/domain\n"
	"    set    : saves the password of name/domain\n"
	"    list   : show the name/domain\n"
	"    unlock : unlocks the keyring.\n"
	"    updatepassword : update all items with the given name/domain.\n"
	"    changekeyringpassword : changes the keyring password.\n"
	"\n WARNING ALL PASSWORD SHOWN IN CLEAR!\n");
	exit(EXIT_FAILURE);
}

char *trim(char *s) {
	char *ptr;
	if (!s)
		return NULL; // handle NULL string
	if (!*s)
		return s; // handle empty string
	for (ptr = s + strlen(s) - 1; (ptr >= s)
			&& (isblank(*ptr) || isspace(*ptr)); --ptr)
		;
	ptr[1] = '\0';
	return s;
}

#define BACKSPACE 127
ssize_t my_getpass(char *lineptr, size_t len, FILE *stream) {
	struct termios old, new;
	int nread = 0;
	char c;

	/* Turn echoing off and fail if we can't. */
	if (tcgetattr(fileno(stream), &old) != 0)
		return -1;
	new = old;
	//	new.c_lflag &= ~ECHO || ECHOCTL;
	new.c_lflag &= ~(ECHO | ECHOE | ECHOK | ECHONL | ICANON);
	if (tcsetattr(fileno(stream), TCSAFLUSH, &new) != 0)
		return -1;
	/* Read the password. */
	while ((c = getchar()) != '\n' && nread + 1 < len) {
		if (c == BACKSPACE) {
			if (nread > 0) {
				nread--;
				printf("\b \b");
			}
		} else {
			lineptr[nread++] = c;
			printf("*");
		}
	}
	printf("\n");
	lineptr[nread] = '\0';

	/* Restore terminal. */
	(void) tcsetattr(fileno(stream), TCSAFLUSH, &old);
	return nread;
}

char * read_password(char * message) {
	char * password;
	fprintf(stdout, "%s", message);
	password = g_malloc(MAX_PASSWORD_LENGTH);

	//	*password = '\0';
	//	fgets(password, MAX_PASSWORD_LENGTH, stdin);

	if (my_getpass(password, MAX_PASSWORD_LENGTH, stdin) == -1) {
		return NULL;
	}
	return trim(password);
}

void check(char * message, char * value) {
	if (value == NULL) {
		fprintf(stderr, "The option --%s is mandatory\n", message);
		exit(1);
	}
}

int askyn() {
	char answer[MAX_PASSWORD_LENGTH];
	answer[0] = '\0';
	char *rc = fgets(answer, MAX_PASSWORD_LENGTH, stdin);
	if (rc && (g_ascii_strcasecmp(answer, "y") != 0 || g_ascii_strcasecmp(answer,
			"yes")) != 0)
		return TRUE;
	else
		return FALSE;
}

void list_items(const char *user, const char *domain) {
	GnomeKeyringResult result;
	GList * found_list;
	GList * i;
	GnomeKeyringNetworkPasswordData * found;

	result = gnome_keyring_find_network_password_sync(user, domain, NULL, NULL,
			NULL, NULL, 0, &found_list);

	if (result == GNOME_KEYRING_RESULT_NO_MATCH) {
		fprintf(stderr, "No matches\n");
		return;
	} else if (result != GNOME_KEYRING_RESULT_OK) {
		fprintf(stderr, "Error in search\n");
		return;
	}

	for (i = found_list; i != NULL; i = i->next) {
		found = i->data;
		printf("%s@%s %s auth %s proto %s", found->user,
				(found->domain != NULL ? found->domain : ""), (found->server
						!= NULL ? found->server : ""), found->authtype, found->protocol);
	}

	gnome_keyring_network_password_list_free(found_list);
}

void change_nitem_password(const char *user, const char *domain,
		const char *newpassword, int ask) {
	GnomeKeyringResult result;
	GList * found_list;
	GList * i;
	GnomeKeyringNetworkPasswordData * found;
	//	char *answer = NULL;
	char *name = NULL;
	int ok;
	guint32 item_id;

	result = gnome_keyring_find_network_password_sync(user, domain, NULL, NULL,
			NULL, NULL, 0, &found_list);

	if (result == GNOME_KEYRING_RESULT_NO_MATCH) {
		fprintf(stderr, "No matches\n");
		return;
	} else if (result != GNOME_KEYRING_RESULT_OK) {
		fprintf(stderr, "Error in search\n");
		return;
	}

	name = g_malloc(MAX_PASSWORD_LENGTH);
	for (i = found_list; i != NULL; i = i->next) {
		found = i->data;
		sprintf(name, "%s@%s %s", found->user,
				(found->domain != NULL ? found->domain : ""), (found->server
						!= NULL ? found->server : ""));
		if (found->password != NULL && g_ascii_strcasecmp(found->password,
				newpassword) != 0) {

			if (ask) {
				fprintf(stdout, "Found %s: Change [y/N]?", name);
				ok = askyn();
			} else
				ok = TRUE;

			if (ok) {
				fprintf(stdout, "Changed %s\n", name);

				result = gnome_keyring_set_network_password_sync(
						found->keyring, found->user, found->domain,
						found->server, found->object, found->protocol,
						found->authtype, found->port, newpassword, &item_id);
				if (result != GNOME_KEYRING_RESULT_OK) {
					fprintf(stderr, "Error updating item\n");
				}
			} else
				fprintf(stdout, "Skipped %s\n", name);
		} else
			fprintf(stdout, "Skipped %s already uptodate\n", name);
	}

	if (name)
		g_free(name);
	gnome_keyring_network_password_list_free(found_list);
}

char * get_password(const char * name, const char * domain) {
	GnomeKeyringAttributeList * attributes;
	GnomeKeyringResult result;
	GList * found_list;
	GList * i;
	GnomeKeyringFound * found;
	char * password = NULL;

	attributes = g_array_new(FALSE, FALSE, sizeof(GnomeKeyringAttribute));
	if (name != NULL)
		gnome_keyring_attribute_list_append_string(attributes, "name", name);
	if (domain != NULL)
		gnome_keyring_attribute_list_append_string(attributes, "domain", domain);
	gnome_keyring_attribute_list_append_string(attributes, "magic",
			PACKAGE_NAME);

	result = gnome_keyring_find_items_sync(GNOME_KEYRING_ITEM_GENERIC_SECRET,
			attributes, &found_list);
	gnome_keyring_attribute_list_free(attributes);

	if (result != GNOME_KEYRING_RESULT_OK)
		return NULL;

	for (i = found_list; i != NULL; i = i->next) {
		found = i->data;
		password = g_strdup(found->secret);
		break;
	}
	gnome_keyring_found_list_free(found_list);

	return password;
}

int set_password(const char * name, const char * domain, const char * password) {
	GnomeKeyringAttributeList * attributes;
	GnomeKeyringResult result;
	guint item_id;

	attributes = g_array_new(FALSE, FALSE, sizeof(GnomeKeyringAttribute));
	gnome_keyring_attribute_list_append_string(attributes, "name", name);
	gnome_keyring_attribute_list_append_string(attributes, "magic",
			PACKAGE_NAME);
	if (domain != NULL)
		gnome_keyring_attribute_list_append_string(attributes, "domain", domain);

	result = gnome_keyring_item_create_sync(NULL,
			GNOME_KEYRING_ITEM_GENERIC_SECRET, name, attributes, password,
			TRUE, &item_id);
	gnome_keyring_attribute_list_free(attributes);

	return (result == GNOME_KEYRING_RESULT_OK);
}

int main(int argc, char * argv[]) {
	enum {
		MODE_GET, MODE_SET, MODE_UNLOCK, MODE_CHANGE_PASSORWD, MODE_UPDATE, MODE_LIST
	} mode;
	char * name = NULL;
	char * domain = NULL;
	char * password = NULL;
	char * cpassword;
	GnomeKeyringResult result;
	GList * i;
	int x;
	int ask = FALSE;

	g_set_application_name(PACKAGE_NAME);

	if (argc < 2)
		usage();

	if (g_ascii_strcasecmp(argv[1], "get") == 0)
		mode = MODE_GET;
	else if (g_ascii_strcasecmp(argv[1], "set") == 0)
		mode = MODE_SET;
	else if (g_ascii_strcasecmp(argv[1], "list") == 0)
		mode = MODE_LIST;
	else if (g_ascii_strcasecmp(argv[1], "unlock") == 0)
		mode = MODE_UNLOCK;
	else if (g_ascii_strcasecmp(argv[1], "updatepassword") == 0)
		mode = MODE_UPDATE;
	else if (g_ascii_strcasecmp(argv[1], "changekeyringpassword") == 0)
		mode = MODE_CHANGE_PASSORWD;
	else if (g_ascii_strcasecmp(argv[1], "--help") == 0)
		usage();
	else {
		fprintf(stderr, "Invalid mode: %s\n", argv[1]);
		usage();
	}

	for (x = 2; x < argc; ++x) {
		if (g_ascii_strcasecmp(argv[x], "--name") == 0)
			name = argv[++x];
		else if (g_ascii_strcasecmp(argv[x], "--domain") == 0)
			domain = argv[++x];
		else if (g_ascii_strcasecmp(argv[x], "--password") == 0)
			password = argv[++x];
		else if (g_ascii_strcasecmp(argv[x], "--ask") == 0)
			ask = TRUE;
		else {
			fprintf(stderr, "Invalid option: %s\n", argv[x]);
			usage();
		}
	}

	//	fprintf(stderr, "IN %s %s %s\n", name, domain, password);

	switch (mode) {
	case MODE_CHANGE_PASSORWD:
		check("password", password);

		cpassword = read_password("Confirm password: ");

		if (g_ascii_strcasecmp(password, cpassword) == 0) {
			result
					= gnome_keyring_change_password_sync(NULL, argv[2],
							password);
			if (result != GNOME_KEYRING_RESULT_OK) {
				fprintf(stderr, "Failed to change keyring password.\n");
				exit(EXIT_FAILURE);
			}
		}
		g_free(cpassword);
		break;

	case MODE_UNLOCK:
		check("password", password);

		result = gnome_keyring_list_keyring_names_sync(&i);
		if (GNOME_KEYRING_RESULT_OK == result) {
			for (; i != NULL; i = i->next) {
				printf("Keyring %s", (char *) i->data);
			}
			gnome_keyring_string_list_free(i);
		}

		result = gnome_keyring_unlock_sync(NULL, password);
		if (result == GNOME_KEYRING_RESULT_ALREADY_UNLOCKED) {
			fprintf(stderr, "Keyring already unlocked.\n");
		} else if (result != GNOME_KEYRING_RESULT_OK) {
			fprintf(stderr, "Failed to unlock keyring. %d \n", result);
			exit(EXIT_FAILURE);
		}
		break;

	case MODE_GET:
		check("name", name);

		password = get_password(name, domain);
		if (!password) {
			fprintf(stderr, "Failed to get password: %s\n", name);
			g_free(password);
			exit(EXIT_FAILURE);
		}

		puts(password);
		g_free(password);
		break;

	case MODE_SET:
		check("name", name);
		check("password", password);

		if (!set_password(name, domain, password)) {
			fprintf(stderr, "Failed to set password: %s\n", name);
			exit(EXIT_FAILURE);
		} else
			fprintf(stdout, "Setted password: %s\n", name);
		break;

	case MODE_UPDATE:
		check("name or --domain", (name==NULL && domain==NULL)?NULL:"");
		check("password", password);

		change_nitem_password(name, domain, password, ask);
		break;
	case MODE_LIST:
		check("name or --domain", (name==NULL && domain==NULL)?NULL:"");
		list_items(name, domain);
		break;
	}

	return 0;
}
